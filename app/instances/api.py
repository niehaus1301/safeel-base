import json
import mysql.connector
from flask import Flask
import datetime
import os

import subprocess


app = Flask(__name__)

verbindung = mysql.connector.connect(
    host="localhost",
    user="phpmyadmin",
    passwd="btineuss",
    database="safeel"
)


@app.route('/API/<action>')
@app.route('/API/<action>/<var1>')
@app.route('/API/<action>/<var1>/<var2>')
@app.route('/API/<action>/<var1>/<var2>/<var3>')
def api_reaction(action, var1='', var2='', var3=''):
    meincursor = verbindung.cursor(buffered=True)
    if action == 'getPictureCount':  # Zählt alle Bilder
        try:
            sql = "SELECT MAX(id_picture) FROM pictures"
            meincursor.execute(sql)
            verbindung.commit()
            return (json.dumps(meincursor.fetchone()[0]))
        except:
            return ("Es ist ein Fehler bei getPictureCount aufgetreten")

    elif action == 'getPictureUrl':  # Lokale URL zurueckgeben falls keine angegeben dann letzte
        if not var1:
            sql = "SELECT COUNT(*) FROM pictures"
            meincursor.execute(sql)
            verbindung.commit()
            var1 = meincursor.fetchone()[0]
        try:
            sql = "SELECT filename FROM pictures WHERE id_picture = %s"
            meincursor.execute(sql, [var1])
            verbindung.commit()
            return (json.dumps(meincursor.fetchone()[0]))
        except Exception as e:
            return ("Es ist ein Fehler bei getPictureUrl aufgetreten \n" + str(e))

    elif action == 'getVideoCount':
        try:
            sql = "SELECT MAX(id_video) FROM videos"
            meincursor.execute(sql)
            verbindung.commit()
            return (json.dumps(meincursor.fetchone()[0]))
        except:
            return ("Es ist ein Fehler bei getVideoCount aufgetreten")

    elif action == 'getVideoUrl':
        if not var1:
            sql = "SELECT COUNT(*) FROM videos"
            meincursor.execute(sql)
            verbindung.commit()
            var1 = meincursor.fetchone()[0]
        try:
            sql = "SELECT filename FROM videos WHERE id_video = %s"
            meincursor.execute(sql, [var1])
            verbindung.commit()
            return (json.dumps(meincursor.fetchone()[0]))
        except Exception as e:
            return ("Es ist ein Fehler bei getVideoUrl aufgetreten \n" + str(e))

    elif action == 'savePicture':
        try:
            # Loest ein Foto aus
            # Speichert die URL
            name = str(datetime.datetime.now()).replace(" ", "_")
            p = subprocess.Popen(
                'python3 /home/pi/iotpi/app/scripts/cameraActions.py img ' + name, shell=True)
            sql = "INSERT INTO `pictures` (`id_picture`, `sensor_id`, `filename`) VALUES (NULL, %s, %s)"
            val = (1, name + '.jpg')  # Variable URL und base
            meincursor.execute(sql, val)
            verbindung.commit()
            return ("Erfolgreich gespeichert")
        except Exception as e:
            return ("Es ist ein Fehler bei savePicture aufgetreten \n" + str(e))

    elif action == 'saveVideo':
        try:
            # Loest das Video fuer 30s aus
            # Speichert die URL
            name = str(datetime.datetime.now()).replace(" ", "_")
            p = subprocess.Popen(
                'python3 /home/pi/iotpi/app/scripts/cameraActions.py video ' + name + ' 30', shell=True)
            sql = "INSERT INTO `videos` (`id_video`, `sensor_id`, `filename`) VALUES (NULL, %s, %s)"
            val = (1, name + '.h264')  # Variable URL und base
            meincursor.execute(sql, val)
            verbindung.commit()
            return ("Aufname gestartet")
        except Exception as e:
            return ("Es ist ein Fehler bei saveVideo aufgetreten \n" + str(e))
    else:
        return ("Dieser API-Befehl existiert nicht!")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=4000, debug=True)

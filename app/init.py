# Import initilization-related python modules
import os
import subprocess

# Import hardware modules
import app.modules.ds18b20 as ds18b20
import app.modules.circuit as circuit

# Import components
import app.components.objectDetection as objectDetection
import app.components.tempTrigger as tempTrigger
import app.components.door as door
import app.components.motionTrigger as motionTrigger

# Import plugins
import app.plugins.handler as handler
import app.plugins.mysql as mysql


# Initialize app
def initialize():
    # Set global
    global main, basedir, temp, motion, objectDetection, handler, tempTrigger, circuit, door, mysql

    # get working dir
    basedir = os.path.dirname(os.path.realpath(__file__))

    # Initilize modules
    temp = ds18b20.module()
    circuit = circuit.module()

    # Initialize components
    door = door.component(21)
    motion = motionTrigger.component()
    tempTrigger = tempTrigger.component(10, 1)
    objectDetection = objectDetection.component(
        '/home/pi/tensorflow1/models/research/object_detection', 'ssdlite_mobilenet_v2_coco_2018_05_09'
    )

    # Initialize plugins
    mysql = mysql.plugin("localhost", "phpmyadmin", "btineuss", "safeel")

    # Do some initilization stuff
    # Start API
    subprocess.run('screen -dmS api python3 ' + basedir +
                   '/instances/api.py', shell=True)

    # Initialized !
    print("IoT-Pi v1.1 initialized!")

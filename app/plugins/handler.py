# import app
#!/usr/bin/python3

from app import init as app

# other imports here
import time
import datetime



def objectDetection():
    print("person erkannt")


def doorTrigger(status):
    print("Door opened: " + str(status))  # true = open
    app.mysql.db.cursor().execute(
        "UPDATE door SET status = %s WHERE sensor_id = 1" % status)
    app.mysql.db.commit()


def moved(status):
    print("Bewegung " + str(status))
    app.mysql.db.cursor().execute(
        "UPDATE move SET status = %s WHERE sensor_id = 1" % status)
    app.mysql.db.commit()


def temp(temp):
    print("Temperatur gelesen: " + str(temp))
    app.mysql.db.cursor().execute("UPDATE temp SET temp = %s WHERE sensor_id = 1" % temp)
    app.mysql.db.commit()


def tempOverChange(dif):
    print("Temperature overchanged: " + str(dif))

#import app
from app import init as app

# other imports here
import os
import sys
import numpy as np
import tensorflow as tf
from picamera import PiCamera
from picamera.array import PiRGBArray


# define component
class component():
    def __init__(self, path, model):
        # initilization tasks here
        # set tensorflow dir && change working dir
        os.chdir(path)
        sys.path.insert(0, path)

        # Import utilites
        from utils import label_map_util
        from utils import visualization_utils as vis_util

        # make vis util available throughout the object
        self.vis_util = vis_util

        # set model paths
        PATH_TO_CKPT = os.path.join(path, model, 'frozen_inference_graph.pb')
        PATH_TO_LABELS = os.path.join(path, 'data', 'mscoco_label_map.pbtxt')

        # Number of classes the object detector can identify
        NUM_CLASSES = 90

        # Load the label map -> To get object names from model numbers
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(
            label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
        # add to self
        self.category_index = label_map_util.create_category_index(categories)

        # Load the Tensorflow model into memory.
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            # add to self
            self.sess = tf.Session(graph=detection_graph)

        # Define input and output tensors
        self.image_tensor = detection_graph.get_tensor_by_name(
            'image_tensor:0')
        self.detection_boxes = detection_graph.get_tensor_by_name(
            'detection_boxes:0')
        self.detection_scores = detection_graph.get_tensor_by_name(
            'detection_scores:0')
        self.detection_classes = detection_graph.get_tensor_by_name(
            'detection_classes:0')

        # Number of objects detected
        self.num_detections = detection_graph.get_tensor_by_name(
            'num_detections:0')

        # initialized!
        print("objectDetection component initialized!")

    def detect(self):
        # init Camera
        # TODO: Figure out why the Camera doesn't work if initilized outside process
        camera = PiCamera()
        # set resolution & framerate to lower resource usage
        camera.resolution = (640, 480)
        camera.framerate = 5

        rawCapture = PiRGBArray(camera, size=(640, 480))
        rawCapture.truncate(0)

        # set exit var
        exitNow = False

        # Runs for each camera frame
        for frame1 in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):

            # Acquire frame and expand frame dimensions to have shape
            frame = np.copy(frame1.array)
            frame.setflags(write=1)
            frame_expanded = np.expand_dims(frame, axis=0)

            # Perform the actual detection by running the model with the image as input
            (boxes, scores, classes, num) = self.sess.run(
                [self.detection_boxes, self.detection_scores,
                    self.detection_classes, self.num_detections],
                feed_dict={self.image_tensor: frame_expanded})

            # Draw the results of the detection
            self.vis_util.visualize_boxes_and_labels_on_image_array(
                frame,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                self.category_index,
                use_normalized_coordinates=True,
                line_thickness=8,
                min_score_thresh=0.40)

            # define objects array and minimum treshold
            objects = []
            threshold = 0.5

            # for each recognized object...
            for index, value in enumerate(classes[0]):
                # ..with minimum treshold
                if scores[0, index] > threshold:
                    # if person is recognized
                    if (self.category_index.get(value)).get('name') == 'person':
                        # break and set exit to True
                        exitNow = True
                        break

            rawCapture.truncate(0)

            if exitNow:
                # close camerea
                camera.close()
                # run handler
                app.handler.objectDetection()
                # exit
                break

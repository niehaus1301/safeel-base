# hardware specific imports here
import RPi.GPIO as GPIO

# other imports here
from multiprocessing import process


# define module
class module():
    def __init__(self):
        # initilization tasks heres
        GPIO.setmode(GPIO.BCM)

        # initialized!
        print("Circuit module initilized!")

    def getStatus(self, pin):
        # set PIN as INPUT
        GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # check if circuit is closed
        status = GPIO.input(pin)
        # cleanup PIN
        GPIO.cleanup(pin)
        # return value
        return status
